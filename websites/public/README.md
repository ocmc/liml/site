
# websites test 
test website files generated using Doxa for deployment to a web server.

The files are © The Orthodox Christian Mission Center

Use of these files is subject to the terms stated in the LICENSE file.

[Doxa](https://doxa.liml.org) is a liturgical software product from the [Orthodox Christian Mission Center](https://ocmc.org). 

[Doxa install link](https://github.com/liturgiko/doxa/releases)
