# The Liturgy in My Language 

Source files for the generation of the LIML website using Doxa.

For information view [Doxa manual](https://doxa.liml.org).

The directory structure is as follows:

```
├── LICENSE
├── README.md
├── assets   
│   ├── css         <--- copied to public during a build
│   ├── favicon.ico <--- copied to public during a build
│   ├── img         <--- copied to public during a build
│   ├── js          <--- copied to public during a build
│   ├── layouts     <--- contains golang templates used to generate html and pdf files
│   └── pdf         <--- provides fonts and formatting information for the generation of PDF files
├── configs
│   └── build.yaml  <--- provides settings used to build a website
├── data
│   └── bolt <--- contains a boltdb database of liturgical texts
├── public  <--- this is generated during a build, and can be automatically served by Gitlab pages
│   ├── booksindex.html <--- provides the user with an index of available liturgical books
│   ├── css
│   ├── favicon.ico
│   ├── h               <--- contains generated html files
│   ├── help.html
│   ├── img
│   ├── index.html      <--- home page
│   ├── indexes         <--- service day indexes
│   ├── js
│   ├── p               <--- contains generated pdf files
│   └── servicesindex.html <--- provides the user with an index of available services by date
└── templates  <--- parsed by doxago. Written in the Liturgical Markup Language (lml)
    ├── Blocks         <--- reusable template parts
    ├── Books          <--- templates for liturgical books
    ├── Dated-Services <--- templates for liturgical services for specific dates
    └── Lectionary     <--- templates for liturgical readings from the Holy Bible
```
